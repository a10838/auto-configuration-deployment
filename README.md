# Auto-Configuration-Deployment

PFE 2021-2022 - Automatic configuration deployment project

## Getting started

### Postgres Database Setup
* Check docker/postgresql folder, prepared a docker image (postgres-auto) based on postgres:14-alpine image, and I set up an init-db script in order to initialize the database with dummy data for the batch to process. To run it `cd docker/postgresql` && `docker-compose -f docker-compose.db.yml up`

### ELK Database Setup
* Docker compose file to run Logstash, Elasticsearch, kibana instances `cd docker/elk` `docker-compose -f docker-compose.elk.yml up `


### Run dev environment 
* `cd utils_script` --> `. run_deps.bash`

### Stop dev environment 
* `cd utils_script` --> `. stop_deps.bash`
