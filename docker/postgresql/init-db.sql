--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

-- Started on 2022-05-27 23:36:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE auto_configuration_db;
--
-- TOC entry 3347 (class 1262 OID 16384)
-- Name: auto_configuration_db; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE auto_configuration_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE auto_configuration_db OWNER TO postgres;

\connect auto_configuration_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3337 (class 0 OID 33007)
-- Dependencies: 210
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.client (id, contact, name, department_id) VALUES (1, 'confdeploybatch@gmail.com', 'cli-spiro', 1);


--
-- TOC entry 3338 (class 0 OID 33014)
-- Dependencies: 211
-- Data for Name: department; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.department (id, description, name) VALUES (1, 'supporter à l''organisation pour la livraison des services à leur client final', 'OLM');
INSERT INTO public.department (id, description, name) VALUES (2, 'supporter l''activité d''automatisation des processus des notifications', 'OBS');
INSERT INTO public.department (id, description, name) VALUES (3, 'Maintenir les serveurs d''authentification vers les API.', 'SPR');


--
-- TOC entry 3339 (class 0 OID 33021)
-- Dependencies: 212
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.service (id, created_date, name, support_contact, department_id) VALUES (1, '2022-05-16 16:54:01.754', 'notif_process', 'notif_process@orn.com', 3);


--
-- TOC entry 3340 (class 0 OID 33028)
-- Dependencies: 213
-- Data for Name: subscription_properties; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.subscription_properties (id, call_rate_limit, history_saved, period_rate_limit, request_timeout) VALUES (1, 500, true, 10, 2);


--
-- TOC entry 3341 (class 0 OID 33033)
-- Dependencies: 214
-- Data for Name: subscription_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.subscription_request (id, created_date, due_date, request_status, client_id, service_id, subscription_properties_id) VALUES (0, '2022-05-27 13:54:01.754', '2022-05-30 13:54:01.754', 1, 1, 1, 1);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 209
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


-- Completed on 2022-05-27 23:36:39

--
-- PostgreSQL database dump complete
--

