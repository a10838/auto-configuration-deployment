# Script to stop ELK stack and PostgreSQL DB without removing the containers
docker-compose -f ../docker/elk/docker-compose.elk.yml stop
docker-compose -f ../docker/postgresql/docker-compose.db.yml stop



