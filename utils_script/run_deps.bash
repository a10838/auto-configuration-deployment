# Script to run ELK stack and PostgreSQL DB
docker-compose -f ../docker/elk/docker-compose.elk.yml  up -d
docker-compose -f ../docker/postgresql/docker-compose.db.yml  up -d
#docker run -d --name=prometheus -p 9090:9090 -v c:\\Data\\Projects\\PFE_2021-2022\\project_source_code\\utils_script\\prom_conf.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml
# launch using linux console
#docker run -d --name=prometheuss -p 9090:9090 -v /mnt/c/Data/Projects/PFE_2021-2022/project_source_code/utils_script/prom_conf.yml:/etc/prometheus/prometheus.yml prom/prometheus --config.file=/etc/prometheus/prometheus.yml


export SPRING_CLOUD_DATAFLOW_FEATURES_STREAMS_ENABLED=false
export SPRING_CLOUD_DATAFLOW_FEATURES_SCHEDULES_ENABLED=true
export SPRING_CLOUD_DATAFLOW_FEATURES_TASKS_ENABLED=true
export spring_datasource_url=jdbc:postgresql://localhost:5432/auto_configuration_db
export spring_datasource_username=postgres
export spring_datasource_password=postgres
export spring_datasource_driverClassName=org.postgresql.Driver
export spring_datasource_initialization_mode=never

java -jar ../docker/cloud_flow/spring-cloud-dataflow-server-2.9.2.jar