\select@language {french}
\contentsline {chapter}{\numberline {I}Introduction g\IeC {\'e}n\IeC {\'e}rale}{10}{chapter.1}
\contentsline {chapter}{\numberline {II}Contexte du projet}{11}{chapter.2}
\contentsline {section}{\numberline {II.1}Pr\IeC {\'e}sentation de l\IeC {\textquoteright }entreprise d\IeC {\textquoteright }accueil}{11}{section.2.1}
\contentsline {section}{\numberline {II.2}\IeC {\'E}tude pr\IeC {\'e}alable}{12}{section.2.2}
\contentsline {subsection}{\numberline {II.2.1}Analyse de l'existant}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {II.2.2}Critique de l'existant}{12}{subsection.2.2.2}
\contentsline {subsection}{\numberline {II.2.3}Solution propos\IeC {\'e}e}{13}{subsection.2.2.3}
\contentsline {section}{\numberline {II.3}Planning pr\IeC {\'e}visionnel}{15}{section.2.3}
\contentsline {section}{\numberline {II.4}Sp\IeC {\'e}cification des besoins}{15}{section.2.4}
\contentsline {subsection}{\numberline {II.4.1}Sp\IeC {\'e}cification des besoins fonctionnels}{15}{subsection.2.4.1}
\contentsline {subsection}{\numberline {II.4.2}Sp\IeC {\'e}cification des besoins non fonctionnels}{16}{subsection.2.4.2}
\contentsline {section}{\numberline {II.5}Identification des acteurs}{16}{section.2.5}
\contentsline {section}{\numberline {II.6}Diagrammes de cas d\IeC {\textquoteright }utilisation}{17}{section.2.6}
\contentsline {subsection}{\numberline {II.6.1}Diagramme de gestion des lancements manuels du job de traitement \IeC {\`a} la demande}{18}{subsection.2.6.1}
\contentsline {subsection}{\numberline {II.6.2}Diagramme de supervision des m\IeC {\'e}triques de performance et des logs des jobs de traitement}{20}{subsection.2.6.2}
\contentsline {section}{\numberline {II.7}Choix du mod\IeC {\`e}le de d\IeC {\'e}veloppement}{20}{section.2.7}
\contentsline {chapter}{\numberline {III}Conception du syst\IeC {\`e}me}{22}{chapter.3}
\contentsline {section}{\numberline {III.1}Mod\IeC {\'e}lisation statique}{22}{section.3.1}
\contentsline {subsection}{\numberline {III.1.1}Diagramme de classes}{22}{subsection.3.1.1}
\contentsline {subsection}{\numberline {III.1.2}Architecture mat\IeC {\'e}rielle de l\IeC {\textquoteright }application}{24}{subsection.3.1.2}
\contentsline {section}{\numberline {III.2}Mod\IeC {\'e}lisation dynamique}{24}{section.3.2}
\contentsline {subsection}{\numberline {III.2.1}Diagrammes de s\IeC {\'e}quences}{24}{subsection.3.2.1}
\contentsline {subsection}{\numberline {III.2.2}Diagramme de s\IeC {\'e}quence du processus automatique de traitement des demandes de souscription}{25}{subsection.3.2.2}
\contentsline {subsection}{\numberline {III.2.3}Diagramme de s\IeC {\'e}quence de l'\IeC {\'e}tape de traitement des demandes de souscription}{25}{subsection.3.2.3}
\contentsline {subsection}{\numberline {III.2.4}Diagramme de s\IeC {\'e}quence de l'\IeC {\'e}tape de g\IeC {\'e}n\IeC {\'e}ration des fichiers de configuration pour les demandes de souscription}{27}{subsection.3.2.4}
\contentsline {subsection}{\numberline {III.2.5}Diagramme de s\IeC {\'e}quence de l'\IeC {\'e}tape de d\IeC {\'e}ploiement des fichiers de configuration}{29}{subsection.3.2.5}
\contentsline {subsection}{\numberline {III.2.6}Diagramme d'activit\IeC {\'e} du processus automatique de traitement}{30}{subsection.3.2.6}
\contentsline {chapter}{\numberline {IV}R\IeC {\'e}alisation du syst\IeC {\`e}me}{32}{chapter.4}
\contentsline {section}{\numberline {IV.1}Environnement de d\IeC {\'e}veloppement}{32}{section.4.1}
\contentsline {subsection}{\numberline {IV.1.1}Environnement logiciel}{32}{subsection.4.1.1}
\contentsline {subsection}{\numberline {IV.1.2}Technologies utilis\IeC {\'e}es}{32}{subsection.4.1.2}
\contentsline {section}{\numberline {IV.2}R\IeC {\'e}alisation du batch de traitement}{34}{section.4.2}
\contentsline {section}{\numberline {IV.3}Dockerisation et composants du projet}{35}{section.4.3}
\contentsline {subsection}{\numberline {IV.3.1}La dockerisation des composants du projet}{35}{subsection.4.3.1}
\contentsline {subsection}{\numberline {IV.3.2}Composants du projet}{37}{subsection.4.3.2}
\contentsline {subsection}{\numberline {IV.3.3}Plateforme Spring Cloud Data Flow }{38}{subsection.4.3.3}
\contentsline {subsection}{\numberline {IV.3.4}Plateforme de monitoring de log ELK }{40}{subsection.4.3.4}
\contentsline {subsection}{\numberline {IV.3.5}Plateforme Sonar Cloud }{41}{subsection.4.3.5}
\contentsline {subsection}{\numberline {IV.3.6}Pipeline d'int\IeC {\'e}gration continue}{42}{subsection.4.3.6}
\contentsline {section}{\numberline {IV.4}Architecture de Spring Batch}{43}{section.4.4}
\contentsline {chapter}{\numberline {V}Conclusion g\IeC {\'e}n\IeC {\'e}rale}{45}{chapter.5}
