package or.hs.autoconf;

import or.hs.autoconf.config.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;


@SpringBootApplication
@Generated
public class AutoConfigurationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoConfigurationApplication.class, args);
    }

    @Bean
    void filesystemRoot()  {
        File directory = new File("src/main/resources/configurations");
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

}
