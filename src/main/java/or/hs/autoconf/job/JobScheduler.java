package or.hs.autoconf.job;


import or.hs.autoconf.config.Generated;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@EnableScheduling
@Generated
public class JobScheduler {

    public static final String TIME_STAMP_KEY = "TIME_STAMP";
    private final JobLauncher jobLauncher;
    private final Job job;

    public JobScheduler(JobLauncher jobLauncher, Job job) {
        this.jobLauncher = jobLauncher;
        this.job = job;
    }

    @Scheduled(cron = "${cron.expression}")
    public void scheduleRun() throws JobExecutionException {
        JobParameters jobParameters = generateJobParameters();
        jobLauncher.run(job, jobParameters);

    }

    private JobParameters generateJobParameters() {
        return new JobParametersBuilder()
                .addString(TIME_STAMP_KEY, Long.toString(System.currentTimeMillis()))
                .toJobParameters();
    }

}