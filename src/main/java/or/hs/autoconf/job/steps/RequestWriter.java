package or.hs.autoconf.job.steps;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.domain.YmlConfiguration;
import or.hs.autoconf.domain.enums.RequestStatus;
import or.hs.autoconf.service.DataService;
import or.hs.autoconf.service.EmailService;
import or.hs.autoconf.service.ObjectMapperService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
@StepScope
@Slf4j
public class RequestWriter implements ItemWriter<YmlConfiguration> {

    public static final String DEPLOYED_MESSAGE = "Hello, the subscription request as %s client for accessing the service %s is deployed successfully. Best Regards";
    private final EmailService emailService;
    private final DataService dataService;


    public RequestWriter(EmailService emailService,DataService dataService) {
        this.emailService = emailService;
        this.dataService = dataService;
    }

    @Override
    public void write(List<? extends YmlConfiguration> list) throws IOException {

        for (YmlConfiguration ymlConfiguration : list) {
            log.info("WRITING : " + ymlConfiguration);
            ObjectMapperService.writeFile(ymlConfiguration);
            log.info("INFORMING : " + ymlConfiguration.getClient().getName());
            dataService.updateStatus(ymlConfiguration.getSubscriptionRequest(), RequestStatus.SUCCEEDED);
            emailService.sendEmail(ymlConfiguration.getClient().getContact(), "Your Subscription Request is deployed", String.format(DEPLOYED_MESSAGE, ymlConfiguration.getClient().getName(), ymlConfiguration.getService().getName()));
        }


    }
}
