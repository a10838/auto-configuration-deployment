package or.hs.autoconf.job.steps;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.domain.YmlConfiguration;
import or.hs.autoconf.domain.enums.RequestStatus;
import or.hs.autoconf.service.DataService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@StepScope
@Slf4j
public class RequestProcessor implements ItemProcessor<SubscriptionRequest, YmlConfiguration> {

    private final DataService dataService;

    public RequestProcessor(DataService dataService) {
        this.dataService = dataService;
    }


    @Override
    public YmlConfiguration process(SubscriptionRequest subscriptionRequest) {
        log.info("Processing : " + subscriptionRequest);
        dataService.updateStatus(subscriptionRequest, RequestStatus.ONGOING);
        return YmlConfiguration.builder().client(subscriptionRequest.getClient()).service(subscriptionRequest.getService()).subscriptionProperties(subscriptionRequest.getSubscriptionProperties()).subscriptionRequest(subscriptionRequest).build();
    }
}
