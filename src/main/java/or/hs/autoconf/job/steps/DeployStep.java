package or.hs.autoconf.job.steps;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.config.Generated;
import or.hs.autoconf.service.FileService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
@StepScope
@Slf4j
@Generated
public class DeployStep implements Tasklet {

    private static final String RESOURCES_ORDER_OUTPUT_YAML = "./";
    private final FileService fileService;

    public DeployStep(FileService fileService) {
    this.fileService = fileService;
    }



    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        File dir = new File(RESOURCES_ORDER_OUTPUT_YAML);
        File[] directoryListing = dir.listFiles();
            for (File file : directoryListing) {
                if(file.getName().contains("cli")){
                    this.fileService.deploy(file);
                }
            }
        return RepeatStatus.FINISHED;
    }
}
