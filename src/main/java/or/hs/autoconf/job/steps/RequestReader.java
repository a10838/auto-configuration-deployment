package or.hs.autoconf.job.steps;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.service.DataService;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Component;

import java.util.Iterator;


@Component
@StepScope
@Slf4j
public class RequestReader implements ItemReader<SubscriptionRequest> {

    Iterator<SubscriptionRequest> subscriptionRequests;

    public RequestReader(DataService dataService) {
        this.subscriptionRequests = dataService.findAllPendingRequests().iterator();
    }

    @Override
    public SubscriptionRequest read() {
        return this.subscriptionRequests.hasNext() ? this.subscriptionRequests.next() : null;
    }
}
