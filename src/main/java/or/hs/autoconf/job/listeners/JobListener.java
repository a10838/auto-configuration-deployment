package or.hs.autoconf.job.listeners;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.config.Generated;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;



@Slf4j
@Generated
public class JobListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        String logMessage = String.format("JOB %s IS STARTED", jobExecution.getJobInstance().getJobName());
        log.info(logMessage);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("JOB HAS COMPLETED SUCCESSFULLY");
        } else if (jobExecution.getStatus() == BatchStatus.FAILED) {
            log.error("JOB HAS FAILED");
        }
    }

}