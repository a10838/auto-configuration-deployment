package or.hs.autoconf.job.listeners;

import lombok.extern.slf4j.Slf4j;
import or.hs.autoconf.config.Generated;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;


@Slf4j
@Generated
public class StepListener implements StepExecutionListener {


    @Override
    public void beforeStep(StepExecution stepExecution) {
        String logMessage = String.format("STEP %s IS STARTED", stepExecution.getStepName());
        log.info(logMessage);

    }


    @Override

    public ExitStatus afterStep(StepExecution stepExecution) {
        String logMessage = String.format("STEP %s IS TERMINATED WITH STATUS : %s", stepExecution.getStepName(), stepExecution.getExitStatus().getExitCode());
        log.info(logMessage);
        return stepExecution.getExitStatus();
    }

}