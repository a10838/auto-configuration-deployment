package or.hs.autoconf.job;


import or.hs.autoconf.config.Generated;
import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.domain.YmlConfiguration;
import or.hs.autoconf.job.listeners.JobListener;
import or.hs.autoconf.job.listeners.StepListener;
import or.hs.autoconf.job.steps.DeployStep;
import or.hs.autoconf.job.steps.RequestProcessor;
import or.hs.autoconf.job.steps.RequestReader;
import or.hs.autoconf.job.steps.RequestWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing
@EnableTask
@Generated
public class JobConfiguration {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final RequestProcessor requestProcessor;
    private final RequestReader requestReader;
    private final RequestWriter requestWriter;
    private final DeployStep deployStep;


    @Autowired
    JobConfiguration(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory,
                     RequestProcessor requestProcessor, RequestReader requestReader,
                     RequestWriter requestWriter, DeployStep deployStep) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
        this.requestProcessor = requestProcessor;
        this.requestReader = requestReader;
        this.requestWriter = requestWriter;
        this.deployStep = deployStep;

    }

    @Bean
    public Job buildDeploymentJob() {
        return jobBuilderFactory.get("conf_deploy" + System.currentTimeMillis())
                .listener(new JobListener())
                .flow(createConfigurationSteps())
                .next(createDeployStep())
                .end().build();
    }

    /**
     * Step that will write the yaml configuration on the filesystem
     */
    public Step createConfigurationSteps  () {
        return stepBuilderFactory.get("createConfigurationStep")
                .<SubscriptionRequest, YmlConfiguration>chunk(1)
                .reader(requestReader)
                .processor(requestProcessor)
                .writer(requestWriter)
                .listener(new StepListener())
                .build();

    }

    /**
     * Step that will deploy the yaml configuration on the remote server
     */
    public Step createDeployStep() {
        return stepBuilderFactory.get("deployConfigurationStep")
                .tasklet(deployStep)
                .listener(new StepListener())
                .build();

    }
}