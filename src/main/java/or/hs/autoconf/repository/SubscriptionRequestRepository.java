package or.hs.autoconf.repository;

import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.domain.enums.RequestStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubscriptionRequestRepository extends CrudRepository<SubscriptionRequest, Long> {

    List<SubscriptionRequest> findAllByRequestStatus(RequestStatus requestStatus);

}