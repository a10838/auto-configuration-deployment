package or.hs.autoconf.service;

import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.domain.enums.RequestStatus;
import or.hs.autoconf.repository.SubscriptionRequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataService {

    private final SubscriptionRequestRepository subscriptionRequestRepository;

    public DataService(SubscriptionRequestRepository subscriptionRequestRepository) {
        this.subscriptionRequestRepository = subscriptionRequestRepository;
    }

    public List<SubscriptionRequest> findAllPendingRequests() {
        return subscriptionRequestRepository.findAllByRequestStatus(RequestStatus.PENDING);
    }

    public void updateStatus(SubscriptionRequest subscriptionRequest, RequestStatus requestStatus) {
        subscriptionRequest.setRequestStatus(requestStatus);
        subscriptionRequestRepository.save(subscriptionRequest);
    }
}
