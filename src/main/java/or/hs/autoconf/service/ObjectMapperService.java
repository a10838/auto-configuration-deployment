package or.hs.autoconf.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import or.hs.autoconf.domain.YmlConfiguration;

import java.io.File;
import java.io.IOException;

public final class ObjectMapperService {

    private ObjectMapperService() {
    }

    private static final String SRC_MAIN_RESOURCES_ORDER_OUTPUT_YAML = "%s.yaml";
    private static final String FILE_NAME = "%s_%s";

    public static void writeFile(YmlConfiguration ymlConfiguration) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
        objectMapper.findAndRegisterModules();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.writeValue(new File(String.format(SRC_MAIN_RESOURCES_ORDER_OUTPUT_YAML, generateConfFileName(ymlConfiguration))), ymlConfiguration);
    }

    public static String generateConfFileName(YmlConfiguration ymlConfiguration) {
        return String.format(FILE_NAME, ymlConfiguration.getClient().getName(), ymlConfiguration.getService().getName());
    }

}
