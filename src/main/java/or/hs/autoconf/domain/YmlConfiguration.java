package or.hs.autoconf.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * Configuration represents the YML file that will be deployed in the SYNCHRO platform to take into
 * consideration the client's request to access a given service.
 */

@AllArgsConstructor
@Getter
@Builder
public class YmlConfiguration {

    private Client client;
    private Service service;
    private SubscriptionProperties subscriptionProperties;
    @JsonIgnore
    private SubscriptionRequest subscriptionRequest;
}
