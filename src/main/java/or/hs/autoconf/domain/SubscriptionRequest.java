package or.hs.autoconf.domain;


import lombok.*;
import or.hs.autoconf.domain.enums.RequestStatus;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 *  A client request subscription to a service with custom subscription properties through a SubscriptionRequest
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@ToString
public class SubscriptionRequest {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @OneToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @OneToOne
    @JoinColumn(name = "service_id")
    private Service service;
    private Timestamp createdDate;
    private Timestamp dueDate;
    /**
     * Represents the subscription request processing status, if it's ongoing, pending, failed, or succeeded.
     */
    @Setter
    private RequestStatus requestStatus;
    /**
     * For each subscription to a service, the client can specify some custom properties of the requests that
     * will be made targeting the given service when the right configuration will be deployed and the client becomes
     * eligible for calling and using the needed service.
     */
    @OneToOne
    private SubscriptionProperties subscriptionProperties;
}
