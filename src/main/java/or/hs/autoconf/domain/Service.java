package or.hs.autoconf.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * A service that clients can request a subscription for.
 */

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String supportContact;
    private Timestamp createdDate;
    @ManyToOne(cascade = {CascadeType.ALL})
    private Department department;
}
