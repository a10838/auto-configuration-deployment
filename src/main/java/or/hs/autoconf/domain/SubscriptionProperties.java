package or.hs.autoconf.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

/**
 * A service subscriptions request properties that are going to be taken into consideration by the API-MANAGEMENT
 * application through the SYNCHRO platform after that the {@link SubscriptionRequest} will be processed by the batch.
 * They will be applied to any request targeting a service made by a {@link Client} through
 * a {@link SubscriptionRequest}.
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@ToString
public class SubscriptionProperties {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    /**
     * Whether to save or not the requests sent through the service subscription.
     */
    private boolean historySaved;
    /**
     * A time period in which a client should establish a connection with a server.
     */
    private int requestTimeout;

    /**
     * RateLimit prevents API usage spikes on a per-subscription basis by limiting the call rate to a specified
     * number callRateLimit per a specified period time {@link #periodRateLimit} in seconds.
     */
    private int callRateLimit;

    /**
     * RateLimit period time in seconds.
     */
    private int periodRateLimit;

    @ToString.Exclude
    @JsonIgnore
    @OneToOne(mappedBy = "subscriptionProperties")
    private SubscriptionRequest subscriptionRequest;
}
