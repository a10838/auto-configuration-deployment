package or.hs.autoconf.domain.enums;

/**
 * The subscription request made by a client for a service processing status.
 */
public enum RequestStatus {
    PENDING,
    ONGOING,
    FAILED,
    SUCCEEDED
}
