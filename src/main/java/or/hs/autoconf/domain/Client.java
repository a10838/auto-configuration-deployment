package or.hs.autoconf.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

/**
 * Represents the client demanding a subscription for a given service
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@ToString
public class Client {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private String contact;
    @ManyToOne(cascade = CascadeType.ALL)
    private Department department;
}
