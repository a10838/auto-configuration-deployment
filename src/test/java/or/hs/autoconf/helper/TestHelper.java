package or.hs.autoconf.helper;

import or.hs.autoconf.domain.*;
import or.hs.autoconf.domain.enums.RequestStatus;

import java.sql.Timestamp;

public class TestHelper {

    public static final Long DEP_ID = 00L;
    public static final String DEP_NAME = "DEP_NAME";
    public static final String DEP_SEC = "DEP_SEC";
    public static final Long CLIENT_TEST_ID = 99L;
    public static final String CLIENT_NAME = "cli-test";
    public static final String TEST_TEST_COM = "test@test.com";
    public static final String SERVICE_NAME = "deploy_notif";
    public static final String SUPPORT_CONTACT = "test@test.com";

    public static YmlConfiguration buildYmlConfiguration(){
        return YmlConfiguration.builder().service(buildService()).subscriptionProperties(buildSubscriptionProperties()).client(buildClient()).subscriptionRequest(buildSubscriptionRequest()).build();
    }


    public static SubscriptionRequest buildSubscriptionRequest(){
        return new SubscriptionRequest(98744L,buildClient(),buildService(),new Timestamp(87999965),new Timestamp(87999965), RequestStatus.PENDING,buildSubscriptionProperties());
    }


    public static Client buildClient(){
        return new Client(CLIENT_TEST_ID, CLIENT_NAME, TEST_TEST_COM,buildDepartment());
    }

    public static Service buildService(){
        return new Service(77L,SERVICE_NAME, SUPPORT_CONTACT,new Timestamp(87999965),buildDepartment());
    }

    public static Department buildDepartment(){
        return new Department(DEP_ID, DEP_NAME, DEP_SEC);
    }

    public static SubscriptionProperties buildSubscriptionProperties(){
        return new SubscriptionProperties(8799L,true,54,12,100,null);
    }


}
