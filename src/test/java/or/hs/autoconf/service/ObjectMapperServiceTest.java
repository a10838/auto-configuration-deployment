package or.hs.autoconf.service;

import or.hs.autoconf.domain.YmlConfiguration;
import or.hs.autoconf.helper.TestHelper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

class ObjectMapperServiceTest {

    public static final String CONF_PATH = "./cli-test_deploy_notif.yaml";

    @AfterAll
    public static void cleanup() throws IOException {
        Files.deleteIfExists(Paths.get(CONF_PATH));
    }

    @Test
   void writeFileTest() throws IOException {
        YmlConfiguration ymlConfiguration = TestHelper.buildYmlConfiguration();
        ObjectMapperService.writeFile(ymlConfiguration);
        Path confPath = Paths.get(CONF_PATH);
        assertThat(confPath).exists();
    }

}