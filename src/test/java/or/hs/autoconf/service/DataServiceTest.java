package or.hs.autoconf.service;

import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.domain.enums.RequestStatus;
import or.hs.autoconf.repository.SubscriptionRequestRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;


class DataServiceTest {

    private static SubscriptionRequestRepository subscriptionRequestRepository;
    private static DataService dataService;

    @BeforeAll
    public static void setup() {
        subscriptionRequestRepository = Mockito.mock(SubscriptionRequestRepository.class);
        dataService = new DataService(subscriptionRequestRepository);
    }

    @Test
    void findAllPendingSubscriptionRequests() {
        // Act.
        dataService.findAllPendingRequests();

        // Assert.
        Mockito.verify(subscriptionRequestRepository).findAllByRequestStatus(RequestStatus.PENDING);
    }

    @Test
    void updateSubscriptionRequestStatus() {
        // Arrange.
        SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
        subscriptionRequest.setRequestStatus(RequestStatus.PENDING);

        // Act.
        dataService.updateStatus(subscriptionRequest, RequestStatus.ONGOING);

        // Assert.
        assertThat(subscriptionRequest.getRequestStatus()).isEqualTo(RequestStatus.ONGOING);
        Mockito.verify(subscriptionRequestRepository).save(subscriptionRequest);
    }
}