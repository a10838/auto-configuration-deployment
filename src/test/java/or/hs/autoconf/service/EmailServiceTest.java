package or.hs.autoconf.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.*;

class EmailServiceTest {

    private JavaMailSender javaMailSender;

    @Test
    void testEmailSend(){
        javaMailSender = Mockito.mock(JavaMailSender.class);
        String toEmail = "test@mail.com";
        String subject = "Test Email";
        String text = "Test Email content body";
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(toEmail);
        msg.setSubject(subject);
        msg.setText(text);

        EmailService emailService = new EmailService(javaMailSender);
        emailService.sendEmail(toEmail,subject,text);

        Mockito.verify(javaMailSender).send(msg);
    }

}