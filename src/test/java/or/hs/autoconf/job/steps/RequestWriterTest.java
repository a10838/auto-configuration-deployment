package or.hs.autoconf.job.steps;

import or.hs.autoconf.helper.TestHelper;
import or.hs.autoconf.service.DataService;
import or.hs.autoconf.service.EmailService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;


class RequestWriterTest {

    public static final String CONF_PATH = "./cli-test_deploy_notif.yaml";
    private EmailService emailService;

    @AfterAll
    public static void cleanup() throws IOException {
        Files.deleteIfExists(Paths.get(CONF_PATH));
    }

    @Test
    void testWriteStep() throws IOException {
        emailService = Mockito.mock(EmailService.class);
        DataService dataService = Mockito.mock(DataService.class);
        RequestWriter requestWriter = new RequestWriter(emailService,dataService);

        requestWriter.write(Collections.singletonList(TestHelper.buildYmlConfiguration()));

        Path confPath = Paths.get(CONF_PATH);
        assertThat(confPath).exists();
        Mockito.verify(dataService).updateStatus(any(),any());
    }

}