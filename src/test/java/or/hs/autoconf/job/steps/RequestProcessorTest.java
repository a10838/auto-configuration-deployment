package or.hs.autoconf.job.steps;

import or.hs.autoconf.domain.YmlConfiguration;
import or.hs.autoconf.helper.TestHelper;
import or.hs.autoconf.service.DataService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

class RequestProcessorTest {

    @Test
    void testProcessStep(){
        DataService dataService = Mockito.mock(DataService.class);
        RequestProcessor requestProcessor = new RequestProcessor(dataService);

        YmlConfiguration ymlConf = requestProcessor.process(TestHelper.buildSubscriptionRequest());

        Mockito.verify(dataService).updateStatus(any(), any());
        assertThat(ymlConf).usingRecursiveComparison().isEqualTo(TestHelper.buildYmlConfiguration());

    }

}