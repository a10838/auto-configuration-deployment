package or.hs.autoconf.job.steps;

import or.hs.autoconf.domain.SubscriptionRequest;
import or.hs.autoconf.helper.TestHelper;
import or.hs.autoconf.service.DataService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RequestReaderTest {

    public static final List<SubscriptionRequest> SUBSCRIPTION_REQUESTS = Collections.singletonList(TestHelper.buildSubscriptionRequest());

    @Test
    void testReadStep(){
        // Arrange.
        DataService dataService = Mockito.mock(DataService.class);
        Mockito.when(dataService.findAllPendingRequests()).thenReturn(SUBSCRIPTION_REQUESTS);
        RequestReader requestReader = new RequestReader(dataService);
        List<SubscriptionRequest> subscriptionRequestList = new ArrayList<>();
        SubscriptionRequest subscriptionRequest;

        // Act.
        while((subscriptionRequest = requestReader.read()) != null){
            subscriptionRequestList.add(subscriptionRequest);
        }

        // Assert.
        Mockito.verify(dataService).findAllPendingRequests();
        assertThat(subscriptionRequestList).isEqualTo(SUBSCRIPTION_REQUESTS);
    }
}